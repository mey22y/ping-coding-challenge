import random
import webapp2
import requests
import json

MY_API_KEY = "jhNIeb9EhDeIwsdM5klXoKKROqWNy5T_3G5zlywpGDMtYboB7d4WHYEPNoeuWysNQXlQIZPd6FaGHQFZexUtAx-MsLAD7pOOgBYfBrjpGE-Z6mNfQ6QbCS1TXP6BW3Yx" 
ENDPOINT = 'https://api.yelp.com/v3/businesses/search'
HEADERS = {
	'Authorization': 'bearer %s' % MY_API_KEY
	}

class yelp(webapp2.RequestHandler):
	def post(self):
		input = self.request.get('text')
		getRestaurant(input, self) #take the input

def getRestaurant(input, self):
	zipcode = input
	PARAMETERS = {
		'term': 'restaurant',
		'location': zipcode, 
		'radius': 1500,
		'limit': 25,
		'open_now': True,
		'sort_by': 'rating'  
	} #25 best rating restaurant in 1500 meters
	response = requests.get(url=ENDPOINT, params=PARAMETERS, headers=HEADERS)
	data = response.json() #dictionary
	res = data['businesses']  
	ran = random.randint(0, len(res) - 1) #random a restaurant
	result = res[ran]

	for i in result: #print out the result with the name, category and detailed location of the restaurant 
		if i == 'name':
			print(result[i] + ' - ')
			self.response.write(result[i] + ' - ')
		elif i == 'categories':
			print(result[i][0]['alias'])
			self.response.write(result[i][0]['alias'])
			self.response.write(' - ')
		elif i == 'location':
			r = result[i]['display_address']
			print(r)
			print(' - ')
			index = 0
			for x in r[:-1]:
				index += 1
				self.response.write(x + ', ')
			self.response.write(r[index])


app = webapp2.WSGIApplication([(r'/foodtoeat', yelp)]) #invoke by entering /foodtoeat


def main():
	from paste import httpserver
	httpserver.serve(app, host='127.0.0.1', port='1234')

if __name__ == '__main__':
	main()



