import webapp2
import requests
import json

MY_API_KEY = "6058b646-03ea-467c-b6e6-8287eb694b4d" 
ENDPOINT = 'https://www.dictionaryapi.com/api/v3/references/spanish/json/'

HEADERS = {
	'Authorization': 'bearer %s' % MY_API_KEY
	}

class dict(webapp2.RequestHandler):
	def post(self):
		input = self.request.get('text')
		getDefinition(input, self) #take the input

def wordInSpanish(input, self):
	word = input
	url = ENDPOINT 
	url += word + '/?key=' + MY_API_KEY
	print(url)
	response = requests.get(url)
	data = response.json() 
	
	res = data[0]['shortdef']
	self.response.write(word + ': ')
	for i in res[:-1]:
		self.response.write(i + '; ')
	self.response.write(res[-1])

app = webapp2.WSGIApplication([(r'/spanish', dict)]) #invoke by entering /foodtoeat

def main():
	from paste import httpserver
	httpserver.serve(app, host='127.0.0.1', port='8080')

if __name__ == '__main__':
	main()



